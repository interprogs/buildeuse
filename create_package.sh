#!/bin/bash

#$1 = name without space
#$2 = full path
#$3 = email
#$4 =  VERSION 1.0.1 etc ...
#$5 = commment
echo "$(tput setaf 1)Setting up $(tput sgr 0) [$(tput setaf 4)$1$(tput sgr 0)]  Folders ...$(tput sgr 0)"

contentfolder=/home/$USER/$1-$4;
buildfolder=$contentfolder/buildeuse/$1_$4/;

if [ -d "$contentfolder" ]; then
rm -rf $contentfolder
sudo -u $USER mkdir -p $contentfolder
sudo -u $USER mkdir -p $buildfolder

fi

if [ ! -d "$contentfolder" ]; then
sudo -u $USER mkdir -p $contentfolder
sudo -u $USER mkdir -p $buildfolder
fi

sudo -u $USER mkdir -p $contentfolder/usr/local/bin
sudo -u $USER mkdir -p $contentfolder/usr/local/bin/$1
sudo -u $USER mkdir -p $contentfolder/usr/share/applications
sudo -u $USER mkdir -p $contentfolder/DEBIAN
echo "$(tput setaf 1)Setting up $(tput sgr 0)[$(tput setaf 4)$1$(tput sgr 0)]Folders $(tput sgr 0) $(tput setaf 2)[OK]$(tput sgr 0)"
cd $contentfolder


echo "$(tput setaf 1)Settings up Spec file.....$(tput sgr 0)"
echo "$(tput setaf 1)Copying Files ...$(tput sgr 0)"

sudo -u $USER cp -r $2* $buildfolder

echo "$(tput setaf 1)Copying Files  $(tput sgr 0) $(tput setaf 2)[OK]$(tput sgr 0cd)"

cd $buildfolder

sudo -u $USER /home/$USER/venv/bin/pyinstaller --additional-hooks-dir=$buildfolder --noconfirm --name $1 main.py  
#sudo -u $USER /home/$USER/venv/bin/pyinstaller --hidden-import=kivymd --noconfirm --name $1 main.py  



chown $USER ./$1.spec
 
sudo -u $USER /home/$USER/venv/bin/pyinstaller $1.spec --noconfirm --hidden-import=kivymd --clean 

echo "$(tput setaf 1)Setting up pyinstaller $(tput sgr 0) $(tput setaf 2)[OK]$(tput sgr 0)"

echo "$(tput setaf 1)Copying Files ...$(tput sgr 0)"

sudo -u $USER cp -r $buildfolder/dist/* $contentfolder/usr/local/bin/

echo "$(tput setaf 1)Copying Files  $(tput sgr 0) $(tput setaf 2)[OK]$(tput sgr 0)"


cd $contentfolder

echo "$(tput setaf 1)Setting up deb stuff ...$(tput sgr 0)"

deb_control="Source: $1
Priority: extra
Maintainer: $USER <$3>
Build-Depends: debhelper (>= 8.0.0)
Standards-Version: 3.9.2
Package: $1
Version: $4
Architecture: amd64
Description: 
"

echo "$deb_control" > $contentfolder/DEBIAN/control
chown $USER $contentfolder/DEBIAN/control

desktop="[Desktop Entry]
Version=1.0
Name=$1
Comment=$5
Exec=/usr/local/bin/$1
Icon=/usr/share/icons/Humanity/apps/32/application-community.svg
Type=Application
Categories=Games;
"

echo "$desktop" > $contentfolder/usr/share/applications/$1.desktop
chmod a+x $contentfolder/usr/share/applications/$1.desktop
chown $USER $contentfolder/usr/share/applications/$1.desktop

sudo -u $USER rm -fr $contentfolder/build/
sudo -u $USER rm -fr $contentfolder/dist/
sudo -u $USER rm -fr $contentfolder/buildeuse/

echo "$(tput setaf 1)Setting up deb stuff $(tput sgr 0) $(tput setaf 2)[OK]$(tput sgr 0)"

echo "$(tput setaf 1)Building deb ...$(tput sgr 0)"

sudo -u $USER dpkg-deb --build $contentfolder

echo "$(tput setaf 1)App $(tput sgr 0)[$(tput setaf 4)$1$(tput sgr 0)] builded ! $(tput sgr 0) $(tput setaf 2)[OK]$(tput sgr 0)"

