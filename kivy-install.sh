#!/bin/bash
set -euo pipefail

echo "$(tput setaf 1)Update running....$(tput sgr 0)"

apt-get update


echo "$(tput setaf 1)Update$(tput sgr 0) $(tput setaf 2)[OK]$(tput sgr 0)"


echo "$(tput setaf 1)Setting up dependencies running ...$(tput sgr 0)"

#from http://kivy.org/docs/installation/installation-linux.html#ubuntu-12-04-with-python-2-7
# Install necessary system packages
sudo apt install -y build-essential mercurial git python2.7 \
python-setuptools python-dev ffmpeg libsdl-image1.2-dev \
libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev libsdl1.2-dev \
libportmidi-dev libswscale-dev libavformat-dev libavcodec-dev zlib1g-dev \
python-pip
#from http://developer.ubuntu.com/2012/02/how-to-prepare-a-compiled-application-for-ubuntu-software-center/
# Install packages necessary for creating .debs
sudo apt-get install -y devscripts lintian dh-make

echo "$(tput setaf 1)Setting up dependencies$(tput sgr 0) $(tput setaf 2)[OK]$(tput sgr 0)"

echo "$(tput setaf 1)Purge old runing ... $(tput sgr 0)"

sudo apt-get remove --purge -y python-virtualenv python-pip

echo "$(tput setaf 1)Purge old$(tput sgr 0) $(tput setaf 2)[OK]$(tput sgr 0)"

echo "$(tput setaf 1)Installing dependencies for kivy running.... $(tput sgr 0)"

sudo easy_install -U pip
sudo pip install -U virtualenv

echo "$(tput setaf 1)Installing cython.... $(tput sgr 0)"

sudo apt-get remove --purge -y cython
sudo pip install -U cython

echo "[OK]$(tput sgr 0)"

echo "$(tput setaf 1)Installing pygame dependencies.... $(tput sgr 0)"

sudo apt-get remove --purge -y python-numpy
sudo pip install -U numpy

echo "[OK]$(tput sgr 0)"

echo "$(tput setaf 1)Installing pygame.... $(tput sgr 0)"

sudo apt-get remove --purge python-pygame

if [ -d "pygame" ]; then
rm -rf pygame
fi
hg clone https://bitbucket.org/pygame/pygame
cd pygame
python2.7 setup.py build
sudo python2.7 setup.py install
cd ..
sudo rm -rf pygame


echo "$(tput setaf 1)Installing dependencies for kivy ...$(tput sgr 0) $(tput setaf 2)[OK]$(tput sgr 0)"


echo "$(tput setaf 1)Creation of virtualenv runing ... $(tput sgr 0)"

rm -rf venv
sudo -u $USER virtualenv -p python2.7 --system-site-packages venv

echo "$(tput setaf 1)Installing dependencies for kivy ...$(tput sgr 0) $(tput setaf 2)[OK]$(tput sgr 0)"

echo "$(tput setaf 1)Installing kivy$(tput sgr 0)[$(tput setaf 4)stable$(tput sgr 0)] runing ... $(tput sgr 0)"
sudo -u $USER venv/bin/pip install kivy
# For the development version of Kivy, use the following command instead
# venv/bin/pip install git+https://github.com/kivy/kivy.git@master

# Install development version of buildozer into the virtualenv
# venv/bin/pip install git+https://github.com/kivy/buildozer.git@master

# Install development version of plyer into the virtualenv
# venv/bin/pip install git+https://github.com/kivy/plyer.git@master

# Install a couple of dependencies for KivyCatalog
sudo -u $USER venv/bin/pip install -U pygments docutils


sudo -u $USER venv/bin/pip install pyinstaller

echo "$(tput setaf 1)Installing kivy$(tput sgr 0)[$(tput setaf 4)stable$(tput sgr 0)] $(tput setaf 2)[OK]$(tput sgr 0) $(tput sgr 0)"





